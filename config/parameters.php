<?php

namespace config;

define("DATABASE_HOST", "localhost");
define("DATABASE_PORT", "");
define("DATABASE_USERNAME", "root");
define("DATABASE_PASSWORD", "root");
define("DATABASE_NAME", "jsonapi-test");
define("DATABASE_CONNECTION_TYPE", "mysql");
