<?php

require_once __DIR__ . '/../config/parameters.php';

use PDO;
use PDOException;

class Connect
{
    private $host = DATABASE_HOST;

    private $port = DATABASE_PORT;

    private $username = DATABASE_USERNAME;

    private $password = DATABASE_PASSWORD;

    private $database = DATABASE_NAME;

    private $connectionType = DATABASE_CONNECTION_TYPE;

    private $dbh;

    private $stmt;

    public function __constructor()
    {
        $this->connect();
    }

    public function connect()
    {
        try {
            $this->dbh = new PDO($this->getDsn(), $this->username, $this->password, $this->getOptions());
        } catch (PDOException $e) {
            print 'Failed connection: ' . $e->getMessage();
        }
    }

    public function getDsn()
    {
        return $this->connectionType
            . ':dbname=' . $this->database
            . ';host=' . $this->host
            . ';port=' . $this->port
        ;
    }

    public function getOptions()
    {
        return array(
            PDO::ATTR_PERSISTENT    => true,
            PDO::ATTR_ERRMODE       => PDO::ERRMODE_EXCEPTION
        );
    }

    public function query($query)
    {
        $this->stmt = $this->dbh->prepare($query);
    }

    public function bind($param, $value, $type = null)
    {
        if (is_null($type)) {
            switch (true) {
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;

                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;

                case is_null($value):
                    $type = PDO::PARAM_NULL;
                    break;

                default:
                    $type = PDO::PARAM_STR;
            }
        }

        $this->stmt->bindValue($param, $value, $type);
    }

    public function execute()
    {
        return $this->stmt->execute();
    }

    public function resultset()
    {
        $this->execute();

        return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function singleresult(){
        $this->execute();

        return $this->stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function rowCount(){
        return $this->stmt->rowCount();
    }
}
